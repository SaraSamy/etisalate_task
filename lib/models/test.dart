//import 'dart:math';
//
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//
//import 'character.dart';
//
//class TestView extends StatefulWidget {
//  @override
//  _MyAppState createState() => new _MyAppState();
//}
//
//
//class _MyAppState extends State<TestView> {
//  var currentPage = (characters.length / 2).toInt();
//  PageController controller;
//
//  List<Character> tempList=characters;
//  @override
//  Widget build(BuildContext context) {
//    double width = MediaQuery.of(context).size.width;
//    return Scaffold(
//      backgroundColor: Colors.blueGrey,
//      body: Center(
//        child: Stack(
//          children: <Widget>[
//            LayoutBuilder(builder: (context, constraints) {
//              List<Widget> cardList = new List();
//              for (int i = 0; i < tempList.length; i++){
//                cardList.add( Container(
//                  width: width - i * 40,
//                  height: 80.0 + (i * 20),
//                  decoration: BoxDecoration(
//                    borderRadius: BorderRadius.all(Radius.circular(10)),
//                    gradient: new LinearGradient(
//                        colors: [
//                          characters[i].colors[0],
//                          characters[i].colors[1],
//                        ],
//                        begin: const FractionalOffset(0.0, 0.0),
//                        end: const FractionalOffset(0.0, 1.0),
//                        stops: [0.0, 1.0],
//                        tileMode: TileMode.clamp),
//                    boxShadow: [
//                      BoxShadow(
//                        color: characters[i].colors[0].withOpacity(0.5),
//                        spreadRadius: 3,
//                        blurRadius: 5,
//                        offset: Offset(0, 0), // changes position of shadow
//                      ),
//                    ],
//                  ),
//                  child: Image.asset(
//                    characters[i].imagePath,
//                    height: 95,
//                    width: 95,
//                  ),
//                ));
//
//              }
//
//              return Stack(
//                alignment: Alignment.center,
//                children: cardList,
//              );
//            }),
//            Positioned.fill(
//              child: PageView.builder(
//                controller: controller,
//                onPageChanged: _onPageChange,
//                itemBuilder: (context, index) {
//                  return Container();
//                },
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Character temp;
//  _onPageChange(int value) {
//    print(currentPage);
//    if (currentPage == characters.length - 1) {
//      setState(() {
//        currentPage = 0;
//      });
//    } else
//      {
//        temp=characters[currentPage];
//        tempList[currentPage]=tempList[currentPage+1];
//        tempList[currentPage+1]=temp;
//        setState(() {
//          currentPage ++;
//        });
//      }
//
//  }
//  bool backDirection = false;
//
//  @override
//  void initState() {
//    super.initState();
//    controller = PageController(
//        initialPage: currentPage, viewportFraction: 0.6, keepPage: true)
//      ..addListener(() {
//        backDirection =
//            controller.position.userScrollDirection == ScrollDirection.forward;
//        if (backDirection && currentPage == 0) {
//          setState(() {
//            currentPage = characters.length - 1;
//            controller.jumpToPage(currentPage);
//          });
//        }
//
//      });
//  }
//
//}


import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../utils.dart';
import 'character.dart';

typedef PageChangedCallback = void Function(double page);
typedef PageSelectedCallback = void Function(int index);

enum ALIGN { LEFT, CENTER, RIGHT }

class VerticalCardPager extends StatefulWidget {
  final List<Character> images;
  final PageChangedCallback onPageChanged;
  final PageSelectedCallback onSelectedItem;
  final TextStyle textStyle;
  final int initialPage;
  final ALIGN align;

  VerticalCardPager(
      {
        @required this.images,
        this.onPageChanged,
        this.textStyle,
        this.initialPage = 2,
        this.onSelectedItem,
        this.align = ALIGN.CENTER});

  @override
  _VerticalCardPagerState createState() => _VerticalCardPagerState();
}

class _VerticalCardPagerState extends State<VerticalCardPager> {
  bool isScrolling = false;
  double currentPosition;
  PageController controller;

  @override
  void initState() {
    super.initState();

    currentPosition = widget.initialPage.toDouble();
    controller = PageController(initialPage: widget.initialPage);

    controller.addListener(() {
      setState(() {
        currentPosition = controller.page;

        if (widget.onPageChanged != null) {
          Future(() => widget.onPageChanged(currentPosition));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return GestureDetector(
//        onVerticalDragEnd: (details) {
//          isScrolling = false;
//        },
//        onVerticalDragStart: (details) {
//          isScrolling = true;
//        },
        onHorizontalDragEnd: (details) {
          isScrolling = false;
        },
        onHorizontalDragStart: (details) {
          isScrolling = true;
        },
        onTapUp: (details) {
          if ((currentPosition - currentPosition.floor()).abs() <= 0.15) {
            int selectedIndex = onTapUp(
                context, constraints.maxHeight, constraints.maxWidth, details);

            if (selectedIndex == 2) {
              if (widget.onSelectedItem != null) {
                Future(() => widget.onSelectedItem(currentPosition.round()));
              }
            } else if (selectedIndex >= 0) {
              int goToPage = currentPosition.toInt() + selectedIndex - 2;
              controller.animateToPage(goToPage,
                  duration: Duration(milliseconds: 300),
                  curve: Curves.easeInOutExpo);
            }
          }
        },
        child: Stack(
          children: [
            CardControllerWidget(
              currentPostion: currentPosition,
              cardViewPagerHeight: constraints.maxHeight,
              cardViewPagerWidth: constraints.maxWidth,
            ),
            Positioned.fill(
              child: PageView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: widget.images.length,
                controller: controller,
                itemBuilder: (context, index) {
                  return Container();
                },
              ),
            )
          ],
        ),
      );
    });
  }

  int onTapUp(context, maxHeight, maxWidth, details) {
    final RenderBox box = context.findRenderObject();
    final Offset localOffset = box.globalToLocal(details.globalPosition);

    double dx = localOffset.dx;
    double dy = localOffset.dy;

    for (int i = 0; i < 5; i++) {
      double width = getWidth(maxHeight, i);
      double height = getHeight(maxHeight, i);
      double left = (maxWidth / 2) - (width / 2);
      double top = getCardPositionTop(height, maxHeight, i);
//      double top = getCurrentItem(height, maxHeight, i,currentPostion,cardMaxHeight);

      if (top <= dy && dy <= top + height) {
        if (left <= dx && dx <= left + width) {
          return i;
        }
      }
    }
    return -1;
  }

  double getWidth(maxHeight, i) {
    double cardMaxWidth = maxHeight / 2;
    return cardMaxWidth - 60 * (i - 2).abs();
  }

  double getHeight(maxHeight, i) {
    double cardMaxHeight = maxHeight / 2;

    if (i == 2) {
      return cardMaxHeight;
    } else if (i == 0 || i == 4) {
      return cardMaxHeight - cardMaxHeight * (4 / 5) - 10;
    } else
      return cardMaxHeight - cardMaxHeight * (4 / 5);
  }
}

double getCardPositionTop(double cardHeight, double viewHeight, int i) {
  int diff = (2 - i);
  int diffAbs = diff.abs();

  double basePosition = (viewHeight / 2) - (cardHeight / 2);
  double cardMaxHeight = viewHeight / 2;

  if (diffAbs == 0) {
    return basePosition;
  }
  if (diffAbs == 1) {
    if (diff >= 0) {
      return basePosition - (cardMaxHeight * (6 / 9));
    } else {
      return basePosition + (cardMaxHeight * (6 / 9));
    }
  } else {
    if (diff >= 0) {
      return basePosition - cardMaxHeight * (8 / 9);
    } else {
      return basePosition + cardMaxHeight * (8 / 9);
    }
  }
}

class CardControllerWidget extends StatelessWidget {
  final double currentPostion;
  final double cardMaxWidth;
  final double cardMaxHeight;
  final double cardViewPagerHeight;
  final double cardViewPagerWidth;

  CardControllerWidget(
      {
        this.cardViewPagerWidth,
        @required this.cardViewPagerHeight,
        this.currentPostion,})
      : cardMaxHeight = 250,
        cardMaxWidth =300;

  @override
  Widget build(BuildContext context) {
    List<Widget> cardList = [];
    for (int i = 0; i < characters.length; i++) {
      var cardWidth = max(cardMaxWidth*0.7 - 80 * (currentPostion - i).abs(), 0.0);
      var cardHeight = getCardHeight(i,currentPostion,cardMaxHeight);
      var cardTop = getCurrentItem(cardHeight, cardViewPagerHeight, i,currentPostion,cardMaxHeight);
      Widget card = Positioned.directional(
        textDirection: TextDirection.rtl,
        start: cardTop,
        bottom: cardViewPagerWidth - (cardWidth / 6),
        child: Opacity(
          opacity: getOpacity(i,currentPostion),
          child: Container(
            width: cardWidth,
            height: cardHeight,
            child:Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      gradient: new LinearGradient(
                          colors: [
                            characters[i].colors[0],
                            characters[i].colors[1],
                          ],
                          begin: const FractionalOffset(0.0, 0.0),
                          end: const FractionalOffset(0.0, 1.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp),
                      boxShadow: [
                        BoxShadow(
                          color: characters[i].colors[0].withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 5,
                          offset: Offset(0, 0), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          characters[i].imagePath,
                          height: 95,
                          width: 95,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );

      cardList.add(card);
    }
    return Stack(
      children: cardList,
    );
  }
}
