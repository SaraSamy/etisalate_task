import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'character.dart';

class SimpleCarouselSlider extends StatefulWidget {
  final double height;

  const SimpleCarouselSlider({Key key, this.height}) : super(key: key);

  @override
  _SimpleCarouselSliderState createState() => _SimpleCarouselSliderState();
}

class _SimpleCarouselSliderState extends State<SimpleCarouselSlider> {
  int currentPage = (characters.length / 2).toInt();
  PageController controller;
  bool backDirection = false;

  @override
  void initState() {
    super.initState();
    controller = PageController(
        initialPage: currentPage, viewportFraction: 0.6, keepPage: true)
      ..addListener(() {
        backDirection =
            controller.position.userScrollDirection == ScrollDirection.forward;
        if (backDirection && currentPage == 0) {
          setState(() {
            currentPage = characters.length - 1;
            controller.jumpToPage(currentPage);
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      child: PageView.builder(
        controller: controller,
        allowImplicitScrolling: false,
        itemBuilder: (context, idx) {
          int index = characters.length - idx % characters.length - 1;
          return characters.length > 0
              ? itemBuilder(index, idx)
              : Container();
        },
        onPageChanged: _onPageChange,
        pageSnapping: true,
      ),
    );
  }

  Widget itemBuilder(index, realIndex) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1;
        if (controller.position.haveDimensions) {
          value = controller.page - realIndex;
          value = (1 - (value.abs() * 0.5)).clamp(0.0, 1.0);
          return Align(
              alignment: Alignment.center,
              child: Container(
                height: Curves.easeIn.transform(value) * widget.height * 2.0,
                child: child,
              ));
        } else {
          return Align(
              alignment: Alignment.center,
              child: Container(
                height: Curves.easeIn
                        .transform(currentPage == 0 ? value : value * 0.5) *
                    widget.height *
                    2.0,
                child: child,
              ));
        }
      },
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                gradient: new LinearGradient(
                    colors: [
                      characters[index].colors[0],
                      characters[index].colors[1],
                    ],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(0.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
                boxShadow: [
                  BoxShadow(
                    color: characters[index].colors[0].withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    characters[index].imagePath,
                    height: 95,
                    width: 95,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _onPageChange(int value) {
    if (value == characters.length - 1) {
      setState(() {
        currentPage = 0;
      });
    } else
      setState(() {
        currentPage = value;
      });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
}
