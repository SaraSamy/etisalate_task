import 'package:flutter/material.dart';

class Character {
  final String name;
  final String imagePath;
  final String description;
  final List<Color> colors;

  Character(
      {this.name, this.imagePath, this.description, this.colors,});
}



List<Character> characters = [
  Character(
      name: "Cats",
      imagePath: "assets/images/cat.png",
      description:
          "Cat is a mammal belonging to the felidae family",
      colors: [
        Colors.orange.shade200,
        Colors.deepOrange.shade400
      ],
    ),
  Character(
      name: "Birds",
      imagePath: "assets/images/bird.png",
      description:
          "They are the birds that are raised at home and called ornamental birds",
      colors: [
        Colors.green.shade200,
        Colors.lightGreen.shade400
      ],
      ),
  Character(
      name: "Ornamental fish",
      imagePath: "assets/images/fish.png",
      description:
          "They are those creatures with nostrils that lack limbs and fingers",
      colors: [
        Colors.blue.shade200,
        Colors.blueAccent.shade400
      ],
      ),
  Character(
      name: "Camels",
      imagePath: "assets/images/camel.png",
      description:
          "They are animals of the rank of fingers, of the camel specie ",
      colors: [Colors.amber.shade200, Colors.amberAccent.shade400],
      ),
  Character(
      name: "Dogs",
      imagePath: "assets/images/dog.png",
      description:
          "Mammals of the species of dogs of carnivores,",
      colors: [Colors.pink.shade200, Colors.pinkAccent.shade400],
     ),
  Character(
      name: "Horse",
      imagePath: "assets/images/horse.png",
      description:
          "Horses are considered to be mammals that give birth and breastfeed their young from the breed of horse and single-harpoon",
      colors: [Colors.brown.shade200, Colors.brown.shade400],
      ),
];
