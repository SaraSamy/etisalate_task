import 'models/character.dart';

double getOpacity(int i,double currentPostion) {
  double diff = (currentPostion - i);
  if (diff >= -2 && diff <= 2) {
    print("${characters[i].name}     $diff");
    return 1.0;
  } else if (diff > -3 && diff < -2) {
    return 3 - diff.abs();
  } else if (diff > 2 && diff < 3) {
    return 3 - diff.abs();

  } else {
    return 0;
  }
}

double getCurrentItem(double cardHeight, double viewHeight, int i,double currentPostion,double cardMaxHeight) {
  double diff = (currentPostion - i);
  double diffAbs = diff.abs();
  double basePosition = (viewHeight / 5) - (cardHeight / 3);

  if (diffAbs == 0) {
    return basePosition;
  }
  if (diffAbs > 0.0 && diffAbs <= 1.0) {
    if (diff >= 0) {
      return basePosition - (cardMaxHeight * (1/ 7)) * diffAbs;
    } else {
      return basePosition + (cardMaxHeight *0.8) * diffAbs;
    }
  }
  else if (diffAbs > 1.0 && diffAbs < 2.0) {
    if (diff >= 0) {
      return basePosition - (cardMaxHeight * (1 / 3)) -cardMaxHeight * (1 / 3) * (diffAbs - diffAbs.floor()).abs();
    } else {

      return (cardMaxHeight * (1 / 3)) +cardMaxHeight * (1 / 3) * (diffAbs - diffAbs.floor()).abs();
    }
  }
  else {
    if (diff >= 0) {
      return basePosition -70;

    } else {
      return basePosition + cardMaxHeight *0.9;
    }
  }
}

double getCardHeight(int index,double currentPostion,double cardMaxHeight) {
  double diff = (currentPostion - index).abs();
  if (diff >= 0.0 && diff < 1.0) {
    return cardMaxHeight*0.7;
  } else if (diff >= 1.0 && diff < 2.0) {
    return cardMaxHeight - cardMaxHeight * 0.4 - ((diff - diff.floor()));
  } else {
    final height = cardMaxHeight - cardMaxHeight * 0.5;
    return height > 0 ? height : 0;
  }
}
